package net.krinno.todomanager.controller;

import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.krinno.todomanager.model.User;
import net.krinno.todomanager.service.UserService;


/**
 * @author KRINNO
 *
 */

@SessionAttributes("userID")
@Controller
public class LoginController {
	
	static final Logger logger = LoggerFactory.getLogger(LoginController.class);
	
	@Autowired
    private UserService userService;
	
	
	// ROOT	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String root() {
		return "redirect:/login";
	}

	
	// LOGIN	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login() {
		return new ModelAndView("login", "user", new User());
	}
	
	
	// ACCESS DENIED
	@RequestMapping(value = "/denied403", method = RequestMethod.GET)
    public String accessDenied403(Model model) {
        return "denied403";
    }
	
	@RequestMapping(value = "/denied", method = RequestMethod.GET)
    public String accessDenied(Model model) {
		model.addAttribute("error", true);
        return "login";
    }
 	
	// LOGOUT
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout() {
        return "logout";
    }	
	
}