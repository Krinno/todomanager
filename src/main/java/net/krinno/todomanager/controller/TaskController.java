package net.krinno.todomanager.controller;

import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import javax.validation.Valid;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import net.krinno.todomanager.model.Task;
import net.krinno.todomanager.model.User;
import net.krinno.todomanager.service.TaskService;
import net.krinno.todomanager.service.UserService;


/**
 * @author KRINNO
 *
 */

@Controller
@SessionAttributes("userID")
public class TaskController {
	
	private static final Logger logger = LoggerFactory.getLogger(TaskController.class);
	
	@Autowired
    private TaskService taskService;
	
	@Autowired
    private UserService userService;
	
	
	// SHOW TASKS
	@RequestMapping(value = "/user/tasks", method = RequestMethod.GET)
	public String showTasks(Model model) {		
			logger.info("TODO List home");		
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String login = auth.getName();		      
		User user = userService.getUserByLogin(login);

			logger.info("Got the user!");	
		List<Task> tasksList = user.getTasks();		
		model.addAttribute("tasksList", tasksList);
		model.addAttribute("userID", user.getID());
		
		return "user/tasks";
	}

	
	// DELETE TASK
    @RequestMapping(value = "/user/delete/{taskID}", method = RequestMethod.GET)
    public String deleteTask(@PathVariable Integer taskID,
    						 @ModelAttribute("userID") Integer userID) {

        taskService.deleteTask(taskID);
        logger.info("Task deleted!");
        
        return "redirect:/user/tasks?userID=" + userID;
    }

    
	// ADD NEW TASK
    @RequestMapping(value = "/user/addtask", method = RequestMethod.GET)
	public String addTaskGET(@RequestParam("userID") Integer userID, 
							  Model model) {
    	
    	model.addAttribute("task", new Task());
    		logger.info("New empty task created!");
    	
		return "user/taskadd";
	}	
    
    @RequestMapping(value = "/user/addtask", method = RequestMethod.POST)
    public String addTask(@RequestParam("userID") Integer userID, 
    					  @Valid @ModelAttribute("task") Task task, 
    					  BindingResult bindingResult) {
    	
    	if (bindingResult.hasErrors()) {
            logger.info("Fail. Wrong input data. Returning...");
            
            return "user/taskadd";
            
        } else 
        	logger.info("Input data is OK!");    	
    		taskService.addTask(userID, task);
    		logger.info("Adding a new task is done!");    	
    		
    	return "redirect:/user/tasks?userID=" + userID;
    }
    
    
	// EDIT EXISTING TASK
    @RequestMapping(value = "/user/edittask", method = RequestMethod.GET)
    public String editTaskGET(@RequestParam("taskID") Integer taskID, 
    						   Model model) {
    	
    		logger.info("Request for edit an existing task...");
    	model.addAttribute("task", taskService.getTask(taskID)); 
    	
       	return "user/taskedit";	
	}
    	
    @RequestMapping(value = "/user/edittask", method = RequestMethod.POST)
    public String editTask(@RequestParam("taskID") Integer taskID,
    					   @Valid @ModelAttribute("task") Task task,
    					   BindingResult bindingResult,
    					   @ModelAttribute("userID") Integer userID) {

    	if (bindingResult.hasErrors()) {
            logger.info("Fail. Wrong input data. Returning...");
            task.setID(taskID);
            return "user/taskedit";
            
        } else 
        	logger.info("Input data is OK!");    	 
    		task.setID(taskID);
    		taskService.editTask(task);    
    		logger.info("Editing is done!");
    		
    	return "redirect:/user/tasks?userID=" + userID;
    }

}
