package net.krinno.todomanager.controller;

import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import javax.validation.Valid;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import net.krinno.todomanager.service.UserService;
import net.krinno.todomanager.model.User;


/**
 * @author KRINNO
 *
 */

@Controller
public class UserController {
	
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	
	@Autowired	
	private UserService userService;

	
	// USERS LIST
	@RequestMapping(value = "/admin/users", method = RequestMethod.GET)
    public String listUsers(Model model) {
		
		List<User> listUsers = this.userService.listUsers();
        model.addAttribute("listUsers", listUsers);
        return "admin/users";
    }
	
    
	// DELETE USER
	@RequestMapping(value = "/admin/users/delete/{ID}", method = RequestMethod.GET)
	public String deleteUser(@PathVariable("ID") Integer ID){
     
		this.userService.deleteUser(ID);
		return "redirect:/admin/users";
	}


    // SIGN UP (ADD USER)
	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String addUserGET(Model model) {
		
    	model.addAttribute("user", new User());
    		logger.info("New empty user created!");   	
		return "signup";
	}
	
    @RequestMapping(value= "/signup", method = RequestMethod.POST)
    public String addUserPOST(@Valid @ModelAttribute("user") User user,
    							BindingResult bindingResult){
    	
    	if (bindingResult.hasErrors()) {
            logger.info("Failed to save user's data. Returning...");
            return "signup";
            
        } else
        	logger.info("Input data is OK!");
    	
    	this.userService.addUser(user);
        return "redirect:/signupdone";        
    }
    
	@RequestMapping(value = "/signupdone", method = RequestMethod.GET)
	public String signupDone() {
		return "signupdone";
	}
    
}
