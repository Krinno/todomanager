package net.krinno.todomanager.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
////import org.springframework.beans.factory.annotation.Autowired;
////import org.springframework.beans.factory.annotation.Qualifier;
//import javax.annotation.Resource;
//
//import org.springframework.context.annotation.Bean;
////import org.springframework.security.authentication.AuthenticationManager;
////import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
////import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
////import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.builders.WebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.web.csrf.CsrfFilter;
//import org.springframework.web.filter.CharacterEncodingFilter;


@Configuration
@EnableWebSecurity
//@EnableGlobalMethodSecurity(securedEnabled = true)
//@EnableGlobalMethodSecurity( prePostEnabled = true )
@ImportResource({ "classpath:security-context.xml" })
public class SecurityConfig /*extends WebSecurityConfigurerAdapter*/ {

//	@Resource(name="authService")
//	private UserDetailsService userDetailsService;
//	
////	@Autowired
////	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
////		 auth
////		 	.userDetailsService(userDetailsService);
////		 	.passwordEncoder(bCryptPasswordEncoder());
////	}
//	
//		
//	@Override
//	public void configure(WebSecurity web) throws Exception {
//		web.ignoring().antMatchers("/resources/**");
//	}
//	
//	@Override
//    protected void configure(HttpSecurity httpSecurity) throws Exception {
//		
//		CharacterEncodingFilter filter = new CharacterEncodingFilter();
//		filter.setEncoding("UTF-8");
//		filter.setForceEncoding(true);
//		httpSecurity.addFilterBefore(filter,CsrfFilter.class);
//		
//		httpSecurity
////			.csrf().disable()
//			.exceptionHandling().accessDeniedPage("/denied403")
//			.and()			
//			.authorizeRequests()
//					.antMatchers("/").permitAll()
//					.antMatchers("/login").permitAll()
//					.antMatchers("/signup").permitAll()
//					.antMatchers("/signupdone").permitAll()
//					.antMatchers("/user/**").access("hasRole('ROLE_USER')")
//					.antMatchers("/admin/**").access("hasRole('ROLE_ADMIN')")
////					.antMatchers("/user/**").hasRole("USER")
////	                .antMatchers("/admin/**").access("ADMIN")
//	                .and()
//            .formLogin()
//	            	.loginPage("/login")
//	            	.loginProcessingUrl("/login.do")
//	            	.failureUrl("/denied")					
////              	.usernameParameter("j_username").passwordParameter("j_password").permitAll()
//					.defaultSuccessUrl("/user/tasks")
//					.and()
//            .logout()
//	                .logoutSuccessUrl("/logout")
//	                .invalidateHttpSession( true )
//	                .and()
//	                .sessionManagement().maximumSessions( 1 ); 
//	}
//	
//	
//	 @Bean 
//	 public BCryptPasswordEncoder bCryptPasswordEncoder(){
//		 return new BCryptPasswordEncoder();
//	 }
}
