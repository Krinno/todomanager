package net.krinno.todomanager.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;


/**
 * @author KRINNO
 *
 */

@Configuration
@ComponentScan({ "net.krinno.todomanager.*" })
@Import({ MvcConfig.class, HibernateConfig.class, SecurityConfig.class })
public class AppConfig {

}
