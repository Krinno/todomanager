package net.krinno.todomanager.service;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import net.krinno.todomanager.model.User;
import net.krinno.todomanager.repository.UserRepository;


/**
 * @author KRINNO
 *
 */

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserRepository userRepository;


	@Override
	@Transactional
	public List<User> listUsers() {
		return this.userRepository.listUsers();
	}
	
	@Override
	@Transactional
	public User getUserByID(int userID) {
		return this.userRepository.getUserByID(userID);
	}
	
	@Override
	@Transactional
	public User getUserByLogin(String login) {
		return this.userRepository.getUserByLogin(login);
	}
	
	@Override
	@Transactional
	public void addUser(User user) {
		this.userRepository.addUser(user);
	}

	@Override
	@Transactional
	public void deleteUser(int userID) {
		this.userRepository.deleteUser(userID);
	}
}
