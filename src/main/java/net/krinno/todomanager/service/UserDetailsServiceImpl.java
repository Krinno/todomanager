package net.krinno.todomanager.service;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.List;
import java.util.ArrayList;

import net.krinno.todomanager.model.User;
import net.krinno.todomanager.repository.UserRepository;


/**
 * @author KRINNO
 *
 */

@Service("authService")
public class UserDetailsServiceImpl implements UserDetailsService {
	
	private static final Logger logger = LoggerFactory
			.getLogger(UserDetailsServiceImpl.class);
	
	@Autowired
	private UserRepository userRepository;


	@Transactional
	@Override
	public UserDetails loadUserByUsername(String login) {

		logger.info("Try to load user by Login...");
			
		User user = userRepository.getUserByLogin(login);			
		if(user == null) {
			logger.info("User not found :(");
			throw new UsernameNotFoundException("login not found");
		}
					
		logger.info("Got user!");
		String password = user.getPassword();
		System.out.println(password);
		boolean enabled = user.isEnabled();
		boolean accountNonExpired = true;
		boolean credentialsNonExpired = true;
		boolean accountNonLocked = true;
	
	            
		UserDetails userDetails = new org.springframework.security.core.userdetails.User(
				login, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked,
				getAuthorities(user.getRole().ordinal()) );
		logger.info("UserDetails created!");
		return userDetails;	
	}

	
	public Collection<? extends GrantedAuthority> getAuthorities(int role) {
		List<GrantedAuthority> authList = getGrantedAuthorities(getRoles(role));
		return authList;
	}
	
	public List<String> getRoles(int role) {
		
		List<String> roles = new ArrayList<String>();
		
		if (role == 0) {
			roles.add("ROLE_USER");
			roles.add("ROLE_ADMIN");			
		} else if (role == 1) {
			roles.add("ROLE_USER");
		} else {
			roles.add("ROLE_ANONYMOUS");
		}			
		return roles;
	}
		
	public static List<GrantedAuthority> getGrantedAuthorities(List<String> roles) {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		for (String role : roles) {
			authorities.add(new SimpleGrantedAuthority(role));
		}
		return authorities;
	}
}
