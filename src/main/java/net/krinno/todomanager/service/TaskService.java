package net.krinno.todomanager.service;

import java.util.List;
import net.krinno.todomanager.model.Task;


/**
 * @author KRINNO
 *
 */

public interface TaskService {
	
	public List<Task> getTasks(int userID);
	
	public Task getTask(int taskID);
	
	public void addTask(int userID, Task task);
	
	public void editTask(Task task);
	
	public void deleteTask(int taskID);
	
}
