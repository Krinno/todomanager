package net.krinno.todomanager.service;

import java.util.List;
import net.krinno.todomanager.model.User;


/**
 * @author KRINNO
 *
 */

public interface UserService {
	
	public List<User> listUsers();
	
	public User getUserByID(int userID);
	
	public User getUserByLogin(String login);
	
	public void addUser(User user);
	
	public void deleteUser(int userID);
		
}
