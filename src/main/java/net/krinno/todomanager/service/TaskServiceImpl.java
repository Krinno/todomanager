package net.krinno.todomanager.service;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import net.krinno.todomanager.model.Task;
import net.krinno.todomanager.repository.TaskRepository;


/**
 * @author KRINNO
 *
 */

@Service
public class TaskServiceImpl implements TaskService {
	
	@Autowired
	private TaskRepository taskRepository;
	
	
	@Transactional
	public List<Task> getTasks(int userID) {
		return taskRepository.getTasks(userID);
	}
	
	@Transactional
	public Task getTask(int taskID) {
		return taskRepository.getTask(taskID);
	}	
	
	@Transactional
	public void addTask(int userID, Task task) {
		taskRepository.addTask(userID, task);
	}
	
	@Transactional
	public void editTask(Task task) {
		taskRepository.editTask(task);
	}
	
	@Transactional
	public void deleteTask(int taskID) {
		taskRepository.deleteTask(taskID);
	}
}
