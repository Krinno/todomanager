package net.krinno.todomanager.model;

/**
 * @author KRINNO
 *
 */

public enum TaskStatus {
	
	NOT_DONE, DONE
}
