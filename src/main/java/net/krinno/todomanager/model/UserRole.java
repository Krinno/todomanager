package net.krinno.todomanager.model;

/**
 * @author KRINNO
 *
 */

public enum UserRole {
	
	ADMIN, USER, ANONYMOUS;
}
