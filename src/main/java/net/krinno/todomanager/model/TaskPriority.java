package net.krinno.todomanager.model;

/**
 * @author KRINNO
 *
 */

public enum TaskPriority {	
	
	HIGH, MEDIUM, LOW;
}
