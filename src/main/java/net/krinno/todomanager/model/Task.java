package net.krinno.todomanager.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import java.io.Serializable;
import java.util.Date;


/**
 * Entity bean with JPA annotations.
 * Hibernate provides JPA implementation.
 * @author KRINNO
 * 
 */

@Entity
@Table(name = "TASKS")
public class Task implements Serializable {
	
	private static final long serialVersionUID = 6590700043567877119L;

	@Id
	@Column(name = "ID", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int ID;
	
	@Column(name = "name", nullable = false)
	@NotBlank
	private String name;
	
	@Column(name = "status", nullable = false)
	@NotNull
	@Enumerated(EnumType.STRING)
	private TaskStatus status;
	
	@Column(name = "priority", nullable = false)
	@NotNull
	@Enumerated(EnumType.STRING)
	private TaskPriority priority;
	
	@Column(name = "deadline", nullable = true)
	@DateTimeFormat(iso = ISO.DATE)
	@Temporal(TemporalType.DATE)
	private Date deadline;


	public Integer getID() {
		return ID;
	}

	public void setID(int ID) {
		this.ID = ID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public TaskStatus getStatus() {
		return status;
	}

	public void setStatus(TaskStatus status) {
		this.status = status;
	}

	public TaskPriority getPriority() {
		return priority;
	}

	public void setPriority(TaskPriority priority) {
		this.priority = priority;
	}

	public Date getDeadline() {
		return deadline;
	}

	public void setDeadline(Date deadline) {
		this.deadline = deadline;
	}
	
	
	@Override
	public String toString() {
		return "Task [name=" + name + ", status=" + status + ", priority="
				+ priority + ", deadline=" + deadline + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ID;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		
		Task other = (Task) obj;
		if (ID != other.ID)
			return false;
		return true;
	}
	
}