package net.krinno.todomanager.repository;

import java.util.List;
import net.krinno.todomanager.model.Task;


/**
 * @author KRINNO
 *
 */

public interface TaskRepository {
	
	public List<Task> getTasks(Integer userID);
	
	public Task getTask(Integer taskID);
	
	public void addTask(Integer userID, Task task);
	
	public void editTask(Task task);
	
	public void deleteTask(Integer taskID);

}
