package net.krinno.todomanager.repository;

import org.springframework.stereotype.Repository;
import org.springframework.beans.factory.annotation.Autowired;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.krinno.todomanager.model.Task;
import net.krinno.todomanager.model.User;


/**
 * @author KRINNO
 *
 */

@Repository
public class TaskRepositoryImpl implements TaskRepository {
	
	private static final Logger logger = LoggerFactory.getLogger(TaskRepositoryImpl.class);
	
	@Autowired
	protected SessionFactory sessionFactory;
	

	@Override
	public List<Task> getTasks(Integer userID) {
			logger.info("Try to load a list of tasks...");
			
		Session session = this.sessionFactory.getCurrentSession();
		
		Query query = session.createQuery("FROM User as u WHERE u.ID=" + userID);
		User user = (User) query.uniqueResult();
			logger.info("Got user! Returning tasks...");
		
		return new ArrayList<Task>(user.getTasks());
	}

	@Override
	public Task getTask(Integer taskID) {
		
		Session session = this.sessionFactory.getCurrentSession();
		Task task = (Task) session.get(Task.class, new Integer(taskID));
			logger.info("Got task by its ID!"); 
		  
		return task;
	}

	@Override
	public void addTask(Integer userID, Task task) {
		
		Session session = this.sessionFactory.getCurrentSession();
		
			logger.info("Trying to save task..."); 
		session.save(task);
			logger.info("Task saved!"); 
		
		User existingUser = (User) session.get(User.class, new Integer(userID));
			logger.info("User found, try to save task in his list..."); 
		existingUser.getTasks().add(task);
		session.save(existingUser);
			logger.info("User and his task saved!"); 		
	}

	@Override
	public void editTask(Task task) {
		
		Session session = this.sessionFactory.getCurrentSession();
		Task existingTask = (Task) session.get(Task.class, task.getID());
			logger.info("Start to edit...");

		existingTask.setName(task.getName());
		existingTask.setDeadline(task.getDeadline());
		existingTask.setPriority(task.getPriority());
		existingTask.setStatus(task.getStatus());
			logger.info("Fields edited!");
		
		session.flush();		// TODO Do I need this really...?
		session.save(existingTask); 
			logger.info("Changes saved!");
		
	}

	@Override
	public void deleteTask(Integer taskID) {
		
		Session session = this.sessionFactory.getCurrentSession();
		
		Query query = session.createSQLQuery("DELETE FROM User_Tasks " +
			       "WHERE taskID=" + taskID);		
		query.executeUpdate();
				
        Task task = (Task) session.get(Task.class, new Integer(taskID));
        	logger.info("Trying to delete task...");
        
        if (null != task) { 
        	session.delete(task); 
        }
			logger.info("Task deleted successfully!");
	}

}
