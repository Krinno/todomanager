package net.krinno.todomanager.repository;

import org.springframework.stereotype.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.krinno.todomanager.model.User;
import net.krinno.todomanager.model.UserRole;


/**
 * @author KRINNO
 *
 */

@Repository
public class UserRepositoryImpl implements UserRepository {
	
	private static final Logger logger = LoggerFactory.getLogger(UserRepositoryImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;

 
	@SuppressWarnings("unchecked")
	@Override
	public List<User> listUsers() {
			logger.info("Try to load a list of users...");
		
		Session session = this.sessionFactory.getCurrentSession();
		List<User> usersList = session.createQuery("from User").list();
		
        	logger.info("Users List is loaded!");		
		return usersList;
	}
	

	@Override
	public User getUserByID(Integer userID) {
			logger.info("Try to get user by ID...");
			
		Session session = this.sessionFactory.getCurrentSession();      
		User user = (User) session.get(User.class, new Integer(userID));
		
        	logger.info("Person loaded successfully!");
		return user;
	}
	
	public User getUserByLogin(String login)
			throws UsernameNotFoundException, DataAccessException {
		
		Session session = sessionFactory.getCurrentSession();			
		String hql = "FROM User u WHERE u.login = :login";
		
		Query query = session.createQuery(hql);		
		query.setParameter("login", login);
		
		User user = (User) query.uniqueResult();
		return user;
	}
	

	@Override
	public void addUser(User user) {
		
		user.setEnabled(true); 
		user.setRole(UserRole.USER);	// ROLE_USER
			logger.info("Try to add new user...");
			
		Session session = this.sessionFactory.getCurrentSession();
	    session.persist(user);				
	    // TODO difference between save() and persist() ?
	        logger.info("User saved successfully!");		
	}
	
	
	@Override
	public void deleteUser(Integer userID) {
			logger.info("Trying to delete user...");
			
		Session session = this.sessionFactory.getCurrentSession();
		User user = (User) session.load(User.class, new Integer(userID));
		
        if(null != user){
            session.delete(user);
        }
        	logger.info("User deleted successfully!");
	}

}
