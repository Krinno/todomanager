package net.krinno.todomanager.repository;

import java.util.List;
import net.krinno.todomanager.model.User;


/**
 * @author KRINNO
 *
 */

public interface UserRepository {
	
	public List<User> listUsers();
	
	public User getUserByID(Integer userID);
	
	public User getUserByLogin(String login);
	
	public void addUser(User user);
	
	public void deleteUser(Integer userID);
	
}
