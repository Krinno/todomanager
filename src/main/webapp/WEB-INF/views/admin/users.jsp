<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<html>
<head>
    <title>Users Page</title>
    
    <style type="text/css">
        .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;}
        .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
        .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
        .tg .tg-4eph{background-color:#f9f9f9}
    </style>    
</head>

<body>
	<a href="${pageContext.request.contextPath}/logout">Logout</a><br>
	<h2>Users List</h2>

	<c:if test="${pageContext.request.userPrincipal.name != null}">
	Welcome, master ${pageContext.request.userPrincipal.name}!<br><br>
	</c:if>
	
	<c:if test="${!empty listUsers}">
    	<table class="tg">
    		<tr>
		    	<th width="100">Login</th>
		        <th width="120">Name</th>
		        <th width="200">E-mail</th>
		        <th width="60">[Delete]</th>
    		</tr>
    		
    	<c:forEach items="${listUsers}" var="user">
        	<tr>
	        	<td>${user.login}</td>
	            <td>${user.name}</td>
	            <td>${user.email}</td>
	            <td><a href="<c:url value='users/delete/${user.ID}' />" >Delete</a></td>
        	</tr>
    	</c:forEach>
    	</table>
	</c:if>
	
</body>
</html>