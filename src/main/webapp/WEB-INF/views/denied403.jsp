<!DOCTYPE html>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="../resources/bootstrap/css/bootstrap.css" rel="stylesheet">
		<link href="../resources/style.css" rel="stylesheet">
		<title>Logout</title>
	</head>

	<body>
	<div class="container">
	<div class="row">
	<div class="col-md-offset-4 col-md-4">
		<div class="form-denied">
		<div class="wrapper">

			<h2>Access denied!</h2>
			<h4>Go out of here, you, criminal scum!</h4>
 
			<a href="javascript:history.go(-1);"
				class="btn btn-danger btn-md denied">Go back...
					<i class="glyphicon glyphicon-share-alt"></i></a>
		</div>
		</div>
	</div>
	</div>
	</div>	
	</body>
</html>
