<!DOCTYPE html>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="resources/bootstrap/css/bootstrap.css" rel="stylesheet">
		<link href="resources/style.css" rel="stylesheet">
		<title>Logout</title>
	</head>

	<body>
	<div class="container">
	<div class="row">
	<div class="col-md-offset-4 col-md-4">
		<div class="form-login">
		<div class="wrapper">

			<% session.invalidate(); %>
			<h2>You are now logged out!</h2>
 
			<a href="${pageContext.request.contextPath}/login"
				class="btn btn-primary btn-md">Go back
					<i class="glyphicon glyphicon-share-alt"></i></a>
		</div>
		</div>
	</div>
	</div>
	</div>	
	</body>
</html>