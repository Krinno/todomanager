<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<html>
<head>
	<link href="<c:url value="resources/bootstrap/css/bootstrap.css" />" rel="stylesheet">
	<link href="<c:url value="resources/signup.css" />" rel="stylesheet">
    <title>Sign Up</title>
</head>

<body>
<form:form action="signup" commandName="user">

<div class="container">
<div class="row">
<div class="col-md-4 col-md-offset-4">


<div class="form-login">
	<h4 class="text-center">Join us!</h4>
                    
	<form class="form form-signup" role="form">
	
		<div class="form-group">
			<div class="input-group">
			<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
				<form:input path="login" class="form-control" placeholder="Login" />				
			</div>
				<form:errors path="login" class="alert alert-danger error"/>
		</div>
		
		<div class="form-group">
			<div class="input-group">
			<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
				<form:input path="password" class="form-control" placeholder="Password" />				
			</div>
				<form:errors path="password" class="alert alert-danger error"/>
		</div>

		<div class="form-group">
			<div class="input-group">
			<span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
				<form:input path="email" class="form-control" placeholder="E-mail" />								
			</div>
				<form:errors path="email" class="alert alert-danger error"/>
		</div>
		
		<div class="form-group">
			<div class="input-group">
			<span class="input-group-addon"><span class="glyphicon glyphicon-pencil"></span></span>
				<form:input path="name" class="form-control" placeholder="Name" />				
			</div>
				<form:errors path="name" class="alert alert-danger error"/>
		</div>
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		
		<div class="wrapper">
			<button type="submit" class="btn btn-primary btn-md">Sign Up! 
				<i class="glyphicon glyphicon-share-alt"></i></button>
        </div>
        
	</form>
</div> 
</div>
</div>
</div>


</form:form>
</body>

</html>