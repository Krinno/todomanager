<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
						
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">		
		<title>Edit task</title>
		
		<!-- Bootstrap -->
		<link href="../resources/bootstrap/css/bootstrap.css" rel="stylesheet">
		<link href="../resources/task.css" rel="stylesheet">
	</head>
	
<body>
<div class="container wrapper to-center">
	<div class="row">	
		<h2>Edit task:</h2>

<form:form modelAttribute="task" method="post" action="edittask?taskID=${task.ID}">

			<div class="form-group">
			<form:label path="name">Task:</form:label>
			<form:input path="name" cssClass="form-control"/>
			<form:errors path="name" cssClass="alert alert-danger error"/>
			</div>
			
			<div class="form-group">
			<form:label path="status">Status:</form:label><br>	
			<label class="radio-inline">					
				<form:radiobutton label="NOT DONE" path="status" 
					value="NOT_DONE" checked = "checked"/>&nbsp;
			</label> 
			<label class="radio-inline">	
				<form:radiobutton label="DONE" path="status" value="DONE"/>
			</label> 
			</div>
		
			<div class="form-group">	
			<form:label path="priority">Priority:</form:label>
			<form:select path="priority" cssClass="form-control">
				<form:option value="MEDIUM" label="MEDIUM" />
				<form:option value="HIGH" label="HIGH" />
				<form:option value="LOW" label="LOW" />
			</form:select>
			</div>
		
			<div class="form-group">	
			<form:label path="deadline">Deadline:</form:label>
			<form:input path="deadline" cssClass="form-control" placeholder="YYYY-MM-DD"/>
			<form:errors path="deadline" cssClass="alert alert-danger error"/>
			</div>
		

		<button class="btn btn-primary btn-lg btn-right" type="submit">
		<i class="glyphicon glyphicon-floppy-disk"></i> Save</button>
		
</form:form>
</div>
</div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="resources/bootstrap/js/bootstrap.js"></script>

</body>
</html>