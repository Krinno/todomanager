<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix='sec' uri='http://www.springframework.org/security/tags' %>

<!DOCTYPE html>

<html>
	<head>
		<link href="<c:url value="../resources/bootstrap/css/bootstrap.css" />" rel="stylesheet">
		<link href="<c:url value="../resources/list.css"  />" rel="stylesheet">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>TODO List</title>
	</head>

	<body>
	<div class="container wrapper">
	<div class="row">
	
	<a href="${pageContext.request.contextPath}/logout">Logout</a><br>
	<sec:authorize ifAnyGranted='ROLE_ADMIN'> 
    	<a href='../admin/users'> Admin Home </a> 
    </sec:authorize> 
	
	<h2>Your To-Do list,
	<c:if test="${pageContext.request.userPrincipal.name != null}">
	${pageContext.request.userPrincipal.name}:<br>
	</c:if></h2>
	
	<div class="row">
	<a href="addtask?userID=${userID}" class="btn btn-primary">Add new task</a>
	
	<c:if test="${!empty tasksList}">

	<table  class="table table-bordred table-striped table-margin">	
		<tr>
			<th>Task</th>
			<th>Status</th>
			<th>Priority</th>
			<th>Deadline</th>
			<th>Action</th>
		</tr>
		<c:forEach items="${tasksList}" var="task">
			<tr>
				<td>${task.name}</td>
				
				<c:if test="${task.status == 'DONE'}">
				<td><span class="label label-success">${task.status}</span></td></c:if>
				<c:if test="${task.status == 'NOT_DONE'}">
				<td><span class="label label-warning">${task.status}</span></td></c:if>
				<td>${task.priority}</td>
				<td>${task.deadline}</td>
				<td><div class="btn-group btn-group-xs">
				<a href="edittask?taskID=${task.ID}" class="btn btn-primary">
					<i class="glyphicon glyphicon-pencil"></i> Edit</a>					
				<a href="delete/${task.ID}" class="btn btn-danger">
					<i class="glyphicon glyphicon-remove"></i> Delete</a>						
				</div></td>
			</tr>			
		</c:forEach>
		
	</table>

	</c:if>

	</div>
	</div>	
	</div>
	
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="../resources/bootstrap/js/bootstrap.js"></script>
	
	</body>
</html>