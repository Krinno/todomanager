<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
 <%@page contentType="text/html;charset=UTF-8"%>
<html>
	<head>
    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	
    	<title>TODO Manager</title>

    	<!-- Bootstrap -->
		<link href="resources/bootstrap/css/bootstrap.css" rel="stylesheet">
		<link href="resources/login.css" rel="stylesheet">	
	</head>
  
	<body>

    <div class="container wrapper ver-al">
	<div class="row">
		<div class="col-md-6 col-xs-6">
			<img src="resources/images/hello_pic.png" alt="hello pic" class="helpic">
		</div>
		<div class="col-md-6 col-xs-6 greet">
				<h2>Hello, I`m your ToDo Manager!</h2>
 
		<c:if test="${error == true}"><p class="alert alert-danger">
		Invalid login or password.</p>
		</c:if> 
		

<!-- j_spring_security_check -->

        <form action="j_spring_security_check" method="POST">
        
			<input type="text" name="j_username" placeholder="Login" 
				class="form-control input-sm chat-input" />
			<input type="password" name="j_password" placeholder="Password" 
				class="form-control input-sm chat-input" />
			
			<c:out value="${SPRING_SECURITY_LAST_USERNAME}"/>
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
			<div class="text-center">			
			<button type="submit" class="btn btn-labeled btn-success">
				<span class="btn-label"><i class="glyphicon glyphicon-ok"></i></span>Login</button>
			<button type="reset" class="btn btn-labeled btn-danger">
				<span class="btn-label"><i class="glyphicon glyphicon-remove"></i></span>Clear</button>
			</div>

			<div class="row ent-group decfont">
				<a href="signup" class="btn btn-info btn-block">Sign Up</a>
			</div>
		</form>
		
		</div>
	</div>
	</div> 		
		
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="resources/bootstrap/js/bootstrap.js"></script>
    
    </body>
</html>